package utils;
import core.BankSeller;

import org.rspeer.runetek.adapter.component.InterfaceComponent;
import org.rspeer.runetek.adapter.component.Item;
import org.rspeer.runetek.adapter.scene.Npc;
import org.rspeer.runetek.api.component.InterfaceAddress;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.movement.position.Area;
import org.rspeer.runetek.api.scene.Npcs;
import org.rspeer.runetek.providers.RSGrandExchangeOffer;
import org.rspeer.runetek.api.component.GrandExchange;
import org.rspeer.ui.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.function.Predicate;


public class GEUtils {
    public static final Area GRAND_EXCHANGE = Area.rectangular(3153, 3502, 3176, 3477);
    private static ArrayList<Integer> unExchangeableItems = new ArrayList<>();
    //used to add to & filter against in-case Item.isExchangable doesn't cover all items. Also if some items dont sell for 1gp, member objects etc

    public static Predicate<Item> EXCHANGEABLE() {
        //fails with un-noted notable member object (which should never happen, as script deposits inventory at start)
        return item ->
                !(item.getDefinition().isMembers() && !BankSeller.WORLD.isMembers()) &&
                        item.isExchangeable() &&
                        !unExchangeableItems.contains(item.getNotedId() == -1 ? item.getId() : item.getNotedId());
    }


    public static boolean openGE() {
        while (!GrandExchange.isOpen()) {
            Npc clerk = Npcs.getNearest("Grand Exchange Clerk");
            if (clerk == null) {
                Log.severe("error: could not find g.e clerk - must start script at g.e - exiting");
                return false;
            }
            clerk.interact("Exchange");
        }
        return true;
    }

    public static boolean closeGE() {
        final InterfaceAddress CLOSE_ADDRESS = new InterfaceAddress(() -> Interfaces.getFirst(i -> Arrays.asList(i.getActions()).contains("Close") && i.getMaterialId() == 535));
        InterfaceComponent CLOSE_BUTTON = Interfaces.lookup(CLOSE_ADDRESS);
        if (CLOSE_BUTTON != null && CLOSE_BUTTON.interact("Close")) {
            Log.info("successfully completed Close action on g.e interface");
            return true;
        } else {
            Log.info("failed to complete Close action on g.e interface");
            return false;
        }
    }

    public static void abortAll(RSGrandExchangeOffer[] offers) {
        for (RSGrandExchangeOffer offer : offers) {
            offer.abort();
        }
    }

    public static void collectAll(String Action) {
        //Supported actions: "Collect to inventory", "Collect to bank"
        final InterfaceAddress COLLECT_ADDRESS = new InterfaceAddress(() -> Interfaces.getFirst(i -> Arrays.asList(i.getActions()).contains("Collect to bank")));
        InterfaceComponent COLLECT_BUTTON = Interfaces.lookup(COLLECT_ADDRESS);
        if (COLLECT_BUTTON != null && COLLECT_BUTTON.interact(Action)) {
            Log.info(Action + " action successful");
        }

    }

    public static void updateUnExchangeableItems() {
        //finds the first item in the inventory which is not yet in unExchangeableItems then adds it
        try {
            for (Item item : Inventory.getItems()) {
                if (!unExchangeableItems.contains(item.getId())) {
                    unExchangeableItems.add(item.getId());
                    Log.info(item.getName() + " added to unExchangeableItems");
                    break;
                }
            }
        } catch (Exception e) {
            Log.severe(e.getMessage() + e.getCause());
        }

    }

    public static void updateUnExchangeableItems(int itemId) {
        unExchangeableItems.add(itemId);
    }


}
