package tasks;
import static utils.GEUtils.*;

import org.rspeer.runetek.adapter.component.Item;
import org.rspeer.runetek.api.commons.BankLocation;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.Bank;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;


public class Banking extends Task {
    @Override
    public boolean validate() {
        return Inventory.getItems(EXCHANGEABLE()).length == 0;

    }

    @Override
    public int execute() {
        if (Bank.open(BankLocation.GRAND_EXCHANGE) && Time.sleepUntil(() -> (Bank.isOpen() && Bank.setWithdrawMode(Bank.WithdrawMode.NOTE) && Bank.getWithdrawMode() == Bank.WithdrawMode.NOTE), 20000)) {
            if (Inventory.getItems().length >= 1) {
                Log.info("depositing items");
                Time.sleepUntil(() -> (Bank.depositInventory() && Inventory.getItems().length == 0), 10000);
            }

            if (Time.sleepUntil(() -> Bank.getItems(EXCHANGEABLE()).length > 0, 7000)) {
                //bank.isOpen doesnt mean items in bank are loaded, sleep to handle the common case that it falsely sees 0 items in bank and exits script.

                Item[] exchangeableItems = Bank.getItems(EXCHANGEABLE());
                if (exchangeableItems.length >= 1) {
                    for (int i = 0; !Inventory.isFull() && i < exchangeableItems.length; i++) {
                        String itemName = exchangeableItems[i].getName();
                        Log.info("withdrawing " + itemName);
                        Bank.withdrawAll(itemName);
                        Time.sleepUntil(() -> Inventory.contains(itemName), 4000);
                    }
                }
            } else {
                Log.info("no exchangeable items left in bank, stopping script");
                return -1;
            }
        } else {
            Log.severe("Failed to open bank");
        }


        Time.sleepUntil(() -> Bank.close() && Bank.isClosed(), 10000);
        return 300;

    }

}
