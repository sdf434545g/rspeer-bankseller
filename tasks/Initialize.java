package tasks;
import static utils.GEUtils.*;
import core.BankSeller;

import org.rspeer.runetek.api.Worlds;
import org.rspeer.runetek.api.commons.BankLocation;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.Bank;
import org.rspeer.runetek.api.component.GrandExchange;
import org.rspeer.runetek.api.component.tab.Equipment;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;


public class Initialize extends Task {
    private boolean initialized;

    @Override
    public boolean validate() {
        return !initialized;
    }

    @Override
    public int execute() {
        //other tasks assume we are at g.e so this must pass
        if (GRAND_EXCHANGE.contains(Players.getLocal().getPosition())) {
            if (!BankSeller.WORLD.isMembers()) {
                Log.info("Bot started in f2p world " + Worlds.getCurrent() + ", will ignore members-only items");
            }

            Log.info("Attempting to deposit inv & worn..");
            if (Time.sleepUntil(() -> (Bank.open(BankLocation.GRAND_EXCHANGE) && Bank.isOpen()), 10000)) {
                Bank.depositInventory();
                if (Time.sleepUntil(() -> (Bank.depositEquipment() && Equipment.getItems().length == 0 && Bank.depositInventory() && Inventory.getItems().length == 0), 1000, 10000)) {
                    Time.sleepUntil(() -> (Bank.close() && !Bank.isOpen()), 500, 20000);
                }

                if (openGE()) {
                    if (Time.sleepUntil(GrandExchange::isOpen, 20000)) {
                        Log.info("Attempting to abort and collect existing offers..");
                        abortAll(GrandExchange.getOffers());
                        collectAll("Collect to bank");
                    }
                }
            } else {
                Log.severe("failed to deposit inv & worn");
                return -1;
            }

        } else {
            Log.severe("Error: must start script inside g.e");
            return -1;
        }
        initialized = true;
        return 300;

    }
}
