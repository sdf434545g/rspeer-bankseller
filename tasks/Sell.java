package tasks;
import static utils.GEUtils.*;

import org.rspeer.runetek.adapter.component.Item;
import org.rspeer.runetek.api.Definitions;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.Bank;
import org.rspeer.runetek.api.component.GrandExchange;
import org.rspeer.runetek.api.component.GrandExchangeSetup;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.event.listeners.ChatMessageListener;
import org.rspeer.runetek.event.types.ChatMessageEvent;
import org.rspeer.runetek.event.types.ChatMessageType;
import org.rspeer.runetek.providers.RSGrandExchangeOffer;
import org.rspeer.runetek.providers.RSItemDefinition;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;



public class Sell extends Task implements ChatMessageListener {

    @Override
    public boolean validate() {
        return Inventory.getItems(EXCHANGEABLE()).length > 0;
    }

    @Override
    public int execute() {
        Item[] exchangeableItems = Inventory.getItems(EXCHANGEABLE());
        if (openGE() && Time.sleepUntil(() -> (GrandExchange.isOpen() && GrandExchange.getView() == GrandExchange.View.OVERVIEW), 10000)) {
            Time.sleep(2000);
            if (Time.sleepUntil(() -> (GrandExchange.createOffer(RSGrandExchangeOffer.Type.SELL) && GrandExchange.getView() == GrandExchange.View.SELL_OFFER), 7000)) {
                Time.sleep(2000);
                //need to create sell offer at least once use setItem
                for (Item item : exchangeableItems) {
                    Log.info("attempting to sell " + item.getName());
                    if (Time.sleepUntil(() -> (GrandExchangeSetup.setItem(item.getId()) && GrandExchange.getView() == GrandExchange.View.SELL_OFFER), 500, 4000)) {
                        GrandExchangeSetup.setPrice(1);
                        GrandExchangeSetup.setQuantity(Integer.MAX_VALUE);
                        GrandExchangeSetup.confirm();

                    }

                    for (RSGrandExchangeOffer offer : GrandExchange.getOffers((offer) -> !offer.isEmpty())) {
                        if (!Time.sleepUntil(() -> (offer.getProgress() != RSGrandExchangeOffer.Progress.IN_PROGRESS), 7000)) {
                            //if offer doesn't complete within 7 secs, withdraw it and add that item to unexchangeableItems
                            RSItemDefinition unExchangeableItem = Definitions.getItem(offer.getItemId());
                            Log.info("adding " + unExchangeableItem.getName() + " to unExchangeableItems (failed to sell for 1 gp)");
                            updateUnExchangeableItems(unExchangeableItem.getNotedId() == -1 ? unExchangeableItem.getId() : unExchangeableItem.getNotedId()); //offer returns un-noted item ID, have to differentiate here
                            abortAll(GrandExchange.getOffers());
                            Time.sleepUntil(() -> offer.getProgress() == RSGrandExchangeOffer.Progress.EMPTY || offer.getProgress() == RSGrandExchangeOffer.Progress.FINISHED, 2500);
                        }
                        collectAll("Collect to bank");

                    }
                }
                //GE interface doesn't need to be closed here in order to open bank, the same is not true the other way around
            }
        } else if (GrandExchange.isOpen()) {
            Time.sleepUntil(() -> closeGE() && !GrandExchange.isOpen(), 500, 3000);
        } else if (Bank.isOpen()) {
            Time.sleepUntil(() -> Bank.close() && Bank.isClosed(), 500, 3000);
        }

        return 300;
    }

    @Override
    public void notify(ChatMessageEvent message) {
        if (message.getType() == ChatMessageType.SERVER) {

            switch (message.getMessage()) {

                case "You can't trade that item on the Grand Exchange.":
                    updateUnExchangeableItems();
                    break;

                case "You cannot sell that item until your account is older.":
                    Log.info("You cannot sell that item until your account is older.");
                    updateUnExchangeableItems();
                    break;

                case "You can't set up a sell offer at the moment.": //shouldn't ever happen
                    Log.info("GE slots all in use, attempting to abort & collect");
                    abortAll(GrandExchange.getOffers());
                    collectAll("Collect to bank");
                    break;

                case "Your bank cannot hold your items.":
                    Log.severe("bank is full.");
                    break;

                default:
                    Log.info("SERVER MESSAGE " + message.getMessage());
            }

        }
    }
}





