package core;
import tasks.*;

import org.rspeer.runetek.api.Worlds;
import org.rspeer.runetek.providers.RSWorld;
import org.rspeer.script.ScriptMeta;
import org.rspeer.script.task.Task;
import org.rspeer.script.task.TaskScript;
import org.rspeer.ui.Log;

import java.time.Duration;
import java.time.Instant;


/*
    - Still has issue of last item sold doesnt get collected
    - Start in range of g.e clerk
    - Script might fail to find bank (only happens on start) and gets null-pointer exception, if this happens just re-orient the camera or move character and restart script
    - If at any time in the script your bank is full the script will fail, or possibly loop forever.
    - doesn't sell barrows, ornament items etc (if you recommend a good way implement this i will)
*/

@ScriptMeta(developer = "tor", desc = "Sells items to g.e for 1gp ea", name = "core.BankSeller")
public class BankSeller extends TaskScript {
    private static final Instant START_INSTANT = Instant.now();
    public static final RSWorld WORLD = Worlds.getLocal();
    private static final Task[] TASKS = {new Initialize(), new Banking(), new Sell()};


    @Override
    public void onStart() {
        submit(TASKS);
    }


    @Override
    public void onStop() {
        Log.info("script stopped.");
        Log.info("Time elapsed: " + Duration.between(START_INSTANT, Instant.now()).toMinutes() + " minutes");
    }


}

